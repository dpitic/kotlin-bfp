fun main(args: Array<String>) {
    // For loop implementation
    var fortune = ""
    for (i in 1..10) {
        fortune = getFortuneCookie()
        println("\nYour fortune is: $fortune")
        if (fortune.contains("Take it easy")) break
    }
    // Alternative implementation using repeat()
    repeat(10) {
        fortune = getFortuneCookie()
        println("\nYour fortune is: $fortune")
        // Can't use break outside of a loop; use return instead
        if (fortune.contains("Take it easy")) return
    }
    // Alternative implementation with while loop (preferred)
    while (!fortune.contains("Take it easy")) {
        println("\nYour fortune is: $fortune")
        fortune = getFortuneCookie()
    }
}

fun getFortuneCookie(): String {
    val fortunes = listOf(
        "You will have a great day!",
        "Things will go well for you today",
        "Enjoy a wonderful day of success",
        "Be humble and all will turn out well",
        "Today is a good day for exercising restraint.",
        "Take it easy and enjoy life!",
        "Treasure your friends because they are your greatest fortune."
    )

    return when (val birthday = getBirthday()) {
        in 1..7 -> fortunes[2]
        28, 31 -> fortunes[1]
        else -> fortunes[birthday.rem(fortunes.size)]
    }
}

fun getBirthday(): Int {
    print("\nEnter your birthday: ")
    return readLine()?.toIntOrNull() ?: 1
}

