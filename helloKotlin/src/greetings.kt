fun main(args: Array<String>) {
    // One implementation style
    if (args[0].toInt() < 12)
        println("Good morning, Kotlin")
    else
        println("Good night, Kotlin")

    // Alternative implementation using template strings
    println(
        if (args[0].toInt() < 12) "Good morning, Kotlin" else
            "Good night, Kotlin"
    )
}