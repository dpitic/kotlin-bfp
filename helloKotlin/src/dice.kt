import kotlin.random.Random
import kotlin.random.nextInt

fun main(args: Array<String>) {
    println(rollDice(6))
    println(rollDice(0))

    println(rollDice2(6))
    println(rollDice2(0))

    gamePlay(rollDice2(6))
    gamePlay(rollDice2(0))
}

val rollDice =
    { sides: Int -> if (sides == 0) 0 else Random.nextInt(1..12) }
val rollDice2: (Int) -> Int =
    { sides -> if (sides == 0) 0 else Random.nextInt(sides) + 1 }

fun gamePlay(diceRoll: Int) {
    println(diceRoll)
}