import java.util.*

fun main(args: Array<String>) {
    println("Hello, ${args[0]}!")
    feedTheFish()

    // Check if we can add more fish to the tank
    println("${fitMoreFish(10.0, listOf(3, 3, 3))}")
    println("${fitMoreFish(8.0, listOf(2, 2, 2), hasDecorations = false)}")
    println("${fitMoreFish(9.0, listOf(1, 1, 3), 3)}")
    println("${fitMoreFish(10.0, listOf(), 7, true)}")
}

fun shouldChangeWater(
    day: String,
    temperature: Int = 22,
    dirty: Int = 20
): Boolean {
    return when {
        isTooHot(temperature) -> true
        isDirty(dirty) -> true
        isSunday(day) -> true
        else -> false
    }
}

var dirty = 20

val waterFilter: (Int) -> Int = { dirty -> dirty / 2 }
fun feedFish(dirty: Int) = dirty + 10

fun updateDirty(dirty: Int, operation: (Int) -> Int): Int {
    return operation(dirty)
}

fun dirtyProcessor() {
    dirty = updateDirty(dirty, waterFilter)
    // Pass reference to feedFish(); not trying to call feedFish()
    dirty = updateDirty(dirty, ::feedFish)
    // Last parameter call syntax; lambda goes outside of the function parens.
    dirty = updateDirty(dirty) { dirty -> dirty + 50 }
}

fun isTooHot(temperature: Int) = temperature > 30

fun isDirty(dirty: Int) = dirty > 30

fun isSunday(day: String) = day == "Sunday"

fun feedTheFish() {
    val day = randomDay()
    val food = fishFood(day)
    println("Today is $day and the fish eat $food")
    shouldChangeWater(day, 20, 50)
    shouldChangeWater(day)
    shouldChangeWater(day, dirty = 50)
    shouldChangeWater(day = "Monday")

    if (shouldChangeWater(day)) {
        println("Change the water today")
    }

    dirtyProcessor()
}

fun randomDay(): String {
    val week = listOf(
        "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
        "Saturday", "Sunday"
    )
    return week[Random().nextInt(7)]
}

fun fishFood(day: String): String {
    return when (day) {
        "Monday" -> "flakes"
//        "Tuesday" -> "pellets"
        "Wednesday" -> "redworms"
        "Thursday" -> "granules"
        "Friday" -> "mosquitoes"
//        "Saturday" -> "lettuce"
        "Sunday" -> "plankton"
        else -> "fasting"
    }
}

fun fitMoreFish(
    tankSize: Double,
    currentFish: List<Int>,
    fishSize: Int = 2,
    hasDecorations: Boolean = true
): Boolean {
    // Tank with decorations: total length of fish <= 80% of tank size in
    // gallons.
    // Tank without decorations: total length of fish <= 100% of tank size
    return (tankSize * if (hasDecorations) 0.8 else 1.0) >= (currentFish.sum()
            + fishSize)
}