package pairs

fun main(args: Array<String>) {
    val myBook = Book("The Art of War", "Sun Tzu", 1945)
    val bookTitleAuthor = myBook.getTitleAuthor()
    println("Here is your book ${bookTitleAuthor.first} by " +
            "${bookTitleAuthor.second}.")
    val bookTitleAuthorYear = myBook.getTitleAuthorYear()
    println("Here is your book ${bookTitleAuthorYear.first} by " +
            "${bookTitleAuthorYear.second} written in " +
            "${bookTitleAuthorYear.third}.")
}

class Book(val title: String, val author: String, val year: Int) {
    fun getTitleAuthor(): Pair<String, String> {
        return (title to author)
    }

    fun getTitleAuthorYear(): Triple<String, String, Int> {
        return Triple(title, author, year)
    }
}