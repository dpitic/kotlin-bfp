package collections

fun main(args: Array<String>) {
    val testList = listOf(11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
    println(reverseList(testList))
    println(reverseListKotlin(testList))

    // Idiomatic Kotlin solution; returns a new list
    println(testList.reversed())

    // Mutable list
    val symptoms = mutableListOf(
        "white spots", "red spots", "not eating",
        "bloated", "belly up"
    )
    symptoms.add("white fungus")
    symptoms.remove("white fungus")

    symptoms.contains("red") // false
    symptoms.contains("red spots") // true

    // Crate sublist (view) from first arg, to excluding 2nd arg.
    println(symptoms.subList(4, symptoms.size)) // [belly up]

    // Mathematical operations
    println(listOf(1, 5, 3).sum()) // 9
    println(listOf("a", "b", "cc").sumBy { it.length }) // 4

    // Mapping
    val cures = mapOf("white spots" to "Ich", "red sores" to "hole disease")
    println(cures.get("white spots"))
    println(cures["white spots"])

    println(cures.getOrDefault("bloating", "sorry I don't know"))

    cures.getOrElse("bloating") { "No cure for this" }

    val inventory = mutableMapOf("fish net" to 1)
    inventory.put("tank scrubber", 3)
    inventory.remove("fish net")

    val allBooks = setOf(
        "Macbeth",
        "Romeo and Juliet",
        "Hamlet",
        "A Midsummer Night's Dream"
    )
    val library = mapOf("Shakespearea" to allBooks)
    println(library.any { it.value.contains("Hamlet") })

    val moreBooks = mutableMapOf("Wilhelm Tell" to "Schiller")
    moreBooks.getOrPut("Jungle Book") { "Kipling" }
    moreBooks.getOrPut("Hamlet") { "Shakespeare" }
    println(moreBooks)
}

// Typical list reverse function
fun reverseList(list: List<Int>): List<Int> {
    val result = mutableListOf<Int>()
    for (i in 0..list.size - 1) {
        result.add(list[list.size - i - 1])
    }
    return result
}

// Alternative Kotlin implementation; still not idiomatic Kotlin
fun reverseListKotlin(list: List<Int>): List<Int> {
    val result = mutableListOf<Int>()
    for (i in list.size - 1 downTo 0) {
        result.add(list.get(i))
    }
    return result
}