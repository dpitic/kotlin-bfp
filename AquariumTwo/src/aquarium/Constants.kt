package aquarium

// Value assigned at compile time
const val rocks = 3

// Value assigned can be determined at run time; can be used as return value
val number = 5

fun complexFunctionCall() {}
val result = complexFunctionCall()
//const val result2 = complexFunctionCall() // error

const val CONSTANT = "top-level constant"

// const val only works at the top level or in classes declared with object.
object Constants {
    const val CONSTANT2 = "object constant"
}
// Can be used to define or object that creates only constants.
val foo = Constants.CONSTANT2

// Constants can be defined in a file and imported.
// Constants can be defined inside a class using a companion object.
class MyClass {
    companion object {
        // Created when the object is created
        const val CONSTANT3 = "constant inside companion"
    }
}
