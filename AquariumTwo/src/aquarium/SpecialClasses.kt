package aquarium

// Singleton
object MobyDickWhale {

    val author = "Herman Melville"

    fun jump() {
        // ...
    }
}

// Enumeration; this is also a singleton
enum class Colour(val rgb: Int) {
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF)
}

// Can be subclassed ONLY in the same file in which it is declared.
// Kotlin will know all subclasses statically (at compile time).  Can do extra
// checks.
sealed class Seal

class SeaLion: Seal()
class Walrus: Seal()

fun matchSeal(seal: Seal): String {
    return when (seal) {
        is Walrus -> "walrus"
        is SeaLion -> "sea lion"
    }
}