package aquarium

abstract class AquariumFish {
    abstract val colour: String
}

class Shark: AquariumFish(), FishAction {
    override val colour = "grey"

    override fun eat() {
        println("hunt and eat fish")
    }
}

class Plecostomus: AquariumFish(), FishAction {
    override val colour = "gold"

    override fun eat() {
        println("munch on algae")
    }
}

interface FishAction {
    fun eat()
}