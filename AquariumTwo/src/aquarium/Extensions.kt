package aquarium

fun main(args: Array<String>) {
    staticExample()
}

// Extension function enable adding functionality to a class without having
// access to its source code.
fun String.hasSpaces(): Boolean {
    val found: Char? = this.find { it == ' ' }
    return found != null
}

// Implementation can be optimised; do not need to use 'this', it is implied
fun String.hasSpaces2() = find { it == ' ' } != null

fun extensionsExample() {
    "Does it have spaces?".hasSpaces() // true
}

// Useful for separating the core API of classes
open class AquariumPlant(val colour: String, private val size: Int)

class GreenLeafyPlant(size: Int) : AquariumPlant("green", size)

// Extension functions (helpers) can only access public members.
fun AquariumPlant.isRed() = colour == "Red"

fun AquariumPlant.print() = println("AquariumPlant")
fun GreenLeafyPlant.print() = println("GreenLeafyPlant")

fun staticExample() {
    val plant = GreenLeafyPlant(50)
    plant.print() // GreenLeafyPlant

    val aquariumPlant: AquariumPlant = plant
    aquariumPlant.print() // AquariumPlant
}

// Property extensions
val AquariumPlant.isGreen: Boolean
    get() = colour == "Green"

fun propertyExample() {
    val plant = AquariumPlant("Green", 50)
    plant.isGreen // true
}

// Nullable extension
fun AquariumPlant?.pull() {
    this?.apply {
        println("removing $this")
    }
}

fun nullableExample() {
    val plant: AquariumPlant? = null
    plant.pull() // ok
}