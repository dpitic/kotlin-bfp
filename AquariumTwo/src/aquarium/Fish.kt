package aquarium

class Fish(val friendly: Boolean = true, volumeNeeded: Int) {
    // Properties created for val and var attributes
    // No property created for 'volumeNeeded'

    val size: Int

    // Init {} executed from top to bottom & before any secondary constructors
    init {
        println("first init block")
    }

    constructor() : this(true, 9) {
        println("Running secondary constructor")
    }

    init {
        size = if (friendly) {
            volumeNeeded
        } else {
            volumeNeeded * 2
        }
    }

    init {
        println("constructed fish of size $volumeNeeded final size " +
                "${this.size}")
    }

    init {
        println("last init block")
    }
}

// Consider making a helper function instead of additional constructors
fun makeDefaultFish() = Fish(true, 50)

fun fishExample() {
    val fish = Fish(true, 20)
    println(
        "Is the fish friendly? ${fish.friendly}.  It needs volume" +
                "${fish.size}"
    )
}