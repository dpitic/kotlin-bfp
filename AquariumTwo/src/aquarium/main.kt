package aquarium

fun main(args: Array<String>) {
    buildAquarium()
    makeFish()
}

fun buildAquarium() {
    val myAquarium = Aquarium()
    println(
        "Length: ${myAquarium.length} Width: ${myAquarium.width} " +
                "Height: ${myAquarium.height}"
    )

    myAquarium.height = 80
    println("Height: ${myAquarium.height} cm")
    println("Volume: ${myAquarium.volume} litres")

    val smallAquarium = Aquarium(20, 15, 30)
    println("Small Aquarium: ${smallAquarium.volume} litres")

    val myAquarium2 = Aquarium(numberOfFish = 9)

    println(
        "Small Aquarium 2: ${myAquarium2.volume} litres with " +
                "length ${myAquarium2.length} " +
                "width ${myAquarium2.width} " +
                "height ${myAquarium2.height}"
    )
}

fun feedFish(fish: FishAction) {
    // Make some food
    fish.eat()
}

fun makeFish() {
    val shark = Shark()
    val pleco = Plecostomus()

    println("Shark: ${shark.colour}\nPlecostomous: ${pleco.colour}")

    shark.eat()
    pleco.eat()
}