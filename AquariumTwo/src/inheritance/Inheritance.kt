package inheritance

fun main(args: Array<String>) {
    delegate()
}

fun delegate() {
    val pleco = Plecostomous()
    println("Fish has colour ${pleco.colour}")
    pleco.eat()
}

interface FishAction {
    fun eat()
}

interface FishColour {
    val colour: String
}

// Implement the interface FishColour by delegating all calls to the object
// that implements FishColour.  Every time the colour property is called on
// this class, it will call the colour property on the object that implements
// FishColour.
//class Plecostomous(fishColour: FishColour = GoldColour) : FishAction,
//    FishColour by fishColour {
//    // Delegate colour to FishColour by GoldColour; remove colour property
//    // GoldColour's colour property will be called instead.
////    override val colour: String
////        get() = "gold"
//
//    override fun eat() {
//        println("eat algae")
//    }
//}

// The whole Plecostomous class can be simplified using delegation to the point
// where it no longer needs a body
class Plecostomous(fishColour: FishColour = GoldColour) :
    FishAction by PrintingFishAction("a lot of algae"),
    FishColour by fishColour

// Use interface delegation to provide a colour implementation; need an object
// that knows how to provide a fish colour; a singleton in this case.
object GoldColour : FishColour {
    override val colour: String
        get() = "gold"
}

object RedColour : FishColour {
    override val colour: String
        get() = "red"
}

// All eat() methods print out the food; can make a printing fish action that
// implements a FishAction.  This class has a member variable 'food', so it
// cannot be an 'object' like GoldColour or RedColour, because the 'food'
// property will be different for each object.
class PrintingFishAction(val food: String) : FishAction {
    override fun eat() {
        // Print whatever food is passed in
        println(food)
    }
}