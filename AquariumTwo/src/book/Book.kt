package book

import java.util.*

fun main(args: Array<String>) {
    val myBook = Book("Hamlet", "William Shakespeare", 300)
    println("Can borrow books: ${myBook.canBorrow(4)}")
    myBook.printUrl()

    val puppy = Puppy(myBook)
    while (myBook.pages > 0) {
        puppy.playWithBook()
    }
    println("Number of pages: ${myBook.pages}")
}

const val maxBooksBorrow = 5

open class Book(val title: String, val author: String, var pages: Int) {
    private var currentPage: Int = 1

    open fun readPage() {
        currentPage++
    }

    fun canBorrow(borrowedBooks: Int): Boolean {
        return borrowedBooks < maxBooksBorrow
    }

    fun printUrl() {
        println(Constants.BASE_URL + title + ".html")
        println(BASE_URL + title + ".html")
    }

    companion object {
        val BASE_URL = "http://www.turtlecare.net/"
    }
}

// Extension functions
fun Book.getWeight(): Double {
    return pages * 1.5
}

fun Book.tornPages(number: Int) {
    if (pages >= number) pages -= number else pages = 0
}

object Constants {
    const val BASE_URL = "http://www.turtlecare.net/"
}

class eBook(title: String, author: String, var format: String = "text") :
    Book(title, author, 250) {
    private var wordCount: Int = 0

    override fun readPage() {
        wordCount += 250
    }
}

class Puppy(val book: Book) {
    fun playWithBook() {
        book.tornPages(Random().nextInt(12))
    }
}