package spice

class SimpleSpice {
    var name = "curry"
    var spiciness = "mild"
    val heat: Int
        get() {
            return 5
        }

    init {
        println("Name $name; Spiciness $spiciness; Heat $heat")
    }
}