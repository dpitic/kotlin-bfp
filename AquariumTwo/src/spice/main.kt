package spice

fun main(args: Array<String>) {
    val mySimpleSpice = SimpleSpice()
    println("Name: ${mySimpleSpice.name}\nHeat: ${mySimpleSpice.heat}")

    val spices1 = listOf(
        Spice("curry", "mild"),
        Spice("pepper", "medium"),
        Spice("cayenne", "spicy"),
        Spice("ginger", "mild"),
        Spice("red curry", "medium"),
        Spice("green curry", "mild"),
        Spice("hot pepper", "extremely spicy")
    )

    val spice = Spice("cayenne", spiciness = "spicy")
    println(spice.name)

    val spiceList = spices1.filter { it.heat < 5 }
    println(spiceList)

    val salt = makeSalt()
    println(salt.name)
}