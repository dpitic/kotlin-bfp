package spices

fun main(args: Array<String>) {
    val spice = Curry("curry", "hot")
    println("${spice.name} colour is ${spice.colour}")

    val spiceCabinet = listOf(
        SpiceContainer(Curry("Yellow Curry", "mild")),
        SpiceContainer(Curry("Red Curry", "medium")),
        SpiceContainer(Curry("Green Curry", "spicy"))
    )

    for (element in spiceCabinet) println(element.label)
}

sealed class Spice(
    val name: String,
    val spiciness: String = "mild",
    colour: SpiceColour
) : SpiceColour by colour {
    val heat: Int
        get() {
            return when (spiciness) {
                "mild" -> 1
                "medium" -> 3
                "spicy" -> 5
                "very spicy" -> 7
                "extremely spicy" -> 10
                else -> 0
            }
        }

    abstract fun prepareSpice()
}

enum class Colour(val rgb: Int) {
    RED(0xFF0000), GREEN(0x00FF00), BLUE(0x0000FF),
    YELLOW(0xFFFF00)
}

data class SpiceContainer(var spice: Spice) {
    var label = spice.name
}

interface SpiceColour {
    val colour: Colour
}

// Singleton
object YellowSpiceColour : SpiceColour {
    override val colour = Colour.YELLOW
}

class Curry(
    name: String, spiciness: String,
    colour: SpiceColour = YellowSpiceColour
) : Spice(name, spiciness, colour), Grinder {

    override fun prepareSpice() {
        println("preparing curry")
        grind()
    }

    override fun grind() {
        println("grind curry")
    }
}

interface Grinder {
    fun grind()
}